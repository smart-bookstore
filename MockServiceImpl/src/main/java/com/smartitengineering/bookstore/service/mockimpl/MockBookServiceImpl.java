/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.mockimpl;

import com.smartitengineering.bookstore.domain.Book;
import com.smartitengineering.bookstore.domain.filter.BookFilter;
import com.smartitengineering.bookstore.service.api.BookService;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author modhu7
 */
public class MockBookServiceImpl implements BookService {

    private Integer failCount = 1;
    private int createCount = 0;

    public void create(Book book) {        
        if (createCount == failCount.intValue()) {
            createCount = 0;
            throw new IllegalArgumentException("Destined to fail!");
        }
        createCount++;
    }

    public void update(Book book) {
    }

    public void delete(Book book) {
    }

    public Collection<Book> search(final BookFilter bookFilter) {
        if (bookFilter.getName() == null || bookFilter.getName().length() == 0) {
            bookFilter.setName("NO NAME");
        }
        if (bookFilter.getIsbn() == null || bookFilter.getIsbn().length() == 0) {
            bookFilter.setIsbn("No ISBN");
        }
        Collection<Book> books = new ArrayList<Book>();
        for (int i = 0; i < 5; ++i) {
            Book book = new Book();
            book.setName(new StringBuilder(bookFilter.getName()).append(" ").append(i).toString());
            book.setIsbn(new StringBuilder(bookFilter.getIsbn()).append(" ").append(i).toString());
            
            books.add(book);
        }
        return books;
    }

    public Book getByISBN(String isbn) {
        Book book = new Book();
        String isbn_local;
        if (isbn == null || isbn.length() == 0) {
            isbn_local = "No Unique Short Name";
        } else {
            isbn_local = isbn;
        }
        book.setName("No Name");
        book.setIsbn(isbn_local);
        return book;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }
}
