/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.mockimpl;

import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import com.smartitengineering.bookstore.service.api.PublicationHouseService;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author imyousuf
 */
public class MockPubHouseServiceImpl
        implements PublicationHouseService {

    private Integer failCount = 1;
    private int createCount = 0;

    public void create(PublicationHouse publicationHouse) {
        if (createCount == failCount.intValue()) {
            createCount = 0;
            throw new IllegalArgumentException("Destined to fail!");
        }
        createCount++;
    }

    public void update(PublicationHouse publicationHouse) {
    }

    public void delete(PublicationHouse publicationHouse) {
    }

    public Collection<PublicationHouse> search(
            final PublicationHouseFilter houseFilter) {
        if (houseFilter.getName() == null || houseFilter.getName().length() == 0) {
            houseFilter.setName("No Name");
        }
        if (houseFilter.getUniqueShortString() == null || houseFilter.getUniqueShortString().length() == 0) {
            houseFilter.setUniqueShortString("No Unique Short Name");
        }
        Collection<PublicationHouse> publicationHouses =
                new ArrayList<PublicationHouse>();
        for (int i = 0; i < 5; ++i) {
            PublicationHouse house = new PublicationHouse();
            house.setAddress("NONE");
            house.setContactEmailAddress("NONE@NONE");
            house.setName(new StringBuilder(houseFilter.getName()).append(" ").
                    append(i).toString());
            house.setUniqueShortName(new StringBuilder(houseFilter.getUniqueShortString()).append(" ").append(i).toString());
            publicationHouses.add(house);
        }
        return publicationHouses;
    }

    public PublicationHouse getByUniqueShortName(final String uniqueShortName) {
        PublicationHouse house = new PublicationHouse();
        String shortName;
        if (uniqueShortName == null || uniqueShortName.length() == 0) {
            shortName = "No Unique Short Name";
        } else {
            shortName = uniqueShortName;
        }
        house.setAddress("NONE");
        house.setContactEmailAddress("NONE@NONE");
        house.setName("No Name");
        house.setUniqueShortName(shortName);
        return house;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }
}
