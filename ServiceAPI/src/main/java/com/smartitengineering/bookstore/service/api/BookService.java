/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.api;

import com.smartitengineering.bookstore.domain.Book;
import com.smartitengineering.bookstore.domain.filter.BookFilter;
import java.util.Collection;

/**
 *
 * @author modhu7
 */
public interface BookService {

    public void create(Book book);

    public void update(Book book);

    public void delete(Book book);

    public Collection<Book> search(BookFilter bookFilter);

    public Book getByISBN(String isbn);
}
