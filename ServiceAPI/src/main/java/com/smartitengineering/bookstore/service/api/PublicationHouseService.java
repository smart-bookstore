/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.api;

import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import java.util.Collection;

/**
 *
 * @author imyousuf
 */
public interface PublicationHouseService {

    public void create(PublicationHouse publicationHouse);

    public void update(PublicationHouse publicationHouse);

    public void delete(PublicationHouse publicationHouse);

    public Collection<PublicationHouse> search(PublicationHouseFilter houseFilter);

    public PublicationHouse getByUniqueShortName(String uniqueShortName);
}
