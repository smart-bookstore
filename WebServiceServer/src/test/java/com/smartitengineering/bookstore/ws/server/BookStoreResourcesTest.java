/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.server;

import com.smartitengineering.bookstore.domain.Book;
import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.filter.BookFilter;
import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import com.smartitengineering.bookstore.ws.element.BookElement;
import com.smartitengineering.bookstore.ws.element.BookElements;
import com.smartitengineering.bookstore.ws.element.BookFilterElement;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElement;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElements;
import com.smartitengineering.bookstore.ws.element.PublicationHouseFilterElement;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.config.ApacheHttpClientConfig;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import java.io.File;
import java.net.URI;
import java.util.Collections;
import java.util.Random;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.glassfish.embed.GlassFish;
import org.glassfish.embed.ScatteredWar;

/**
 *
 * @author imyousuf
 */
public class BookStoreResourcesTest
    extends TestCase {

    private static int getPort(int defaultPort) {
        String port = System.getenv("JERSEY_HTTP_PORT");
        if (null != port) {
            try {
                return Integer.parseInt(port);
            }
            catch (NumberFormatException e) {
            }
        }
        return defaultPort;
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/").port(getPort(9998)).path(
            "bookstore").build();
    }
    private static final URI BASE_URI = getBaseURI();
    private GlassFish glassfish;
    private WebResource webResource;
    private Client client;

    public BookStoreResourcesTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp()
        throws Exception {
        super.setUp();
        // Start Glassfish
        glassfish = new GlassFish(BASE_URI.getPort());
        // Deploy Glassfish referencing the web.xml
        ScatteredWar war = new ScatteredWar(BASE_URI.getRawPath(),
            new File("src/test/webapp"),
            new File("src/test/webapp/WEB-INF/unitTestWeb.xml"),
            Collections.singleton(new File("target/classes").toURI().toURL()));
        glassfish.deploy(war);
    }

    @Override
    protected void tearDown()
        throws Exception {
        super.tearDown();
        glassfish.stop();
    }

    public void testResources() {
        doTestPublicationServices();
        doTestBookServices();
        doFileAttachmentTest();
    }

    private void doFileAttachmentTest() {
        try {
            WebResource resource =
                getWebResource().path("pub-house").path("test");
            File file = new File("src/test/resources/Splash-resized.jpg");
            MediaType imageType = new MediaType("image", "jpeg");
            FormDataMultiPart part = new FormDataMultiPart();
            final String fileParamName = "myFile";
            final String intParamName = "integer";
            final String boolParamName = "bool";
            final String strParamName = "text";
            final FormDataBodyPart fileAttachmentPart =
                addFileAttachment(fileParamName, file, imageType);
            fileAttachmentPart.setParent(part);
            part.bodyPart(fileAttachmentPart);
            String sampleText1 = "sample text!";
            String sampleInt1 = "12";
            part.field(intParamName, sampleInt1).field(strParamName, sampleText1);
            ClientResponse response = resource.type(
                MediaType.MULTIPART_FORM_DATA_TYPE).post(
                ClientResponse.class, part);
            System.out.println(response.getResponseStatus().toString());
            assertEquals(Status.OK.getStatusCode(), response.getStatus());
            Form readForm = response.getEntity(Form.class);
            assertEquals(sampleInt1, readForm.getFirst(intParamName));
            assertEquals(sampleText1, readForm.getFirst(strParamName));
            assertEquals(file.getName(), readForm.getFirst("fileName"));
            assertEquals(String.valueOf(file.length()),
                readForm.getFirst("fileSize"));
            assertEquals(String.valueOf(file.length()), readForm.getFirst(
                "readFileSize"));
            response.close();
            String sampleText2 = "sample text 2!";
            String sampleInt2 = "13";
            String sampleBool = "true";
            Form form = new Form();
            form.add(intParamName, sampleInt2);
            form.add(strParamName, sampleText2);
            form.add(boolParamName, sampleBool);
            response = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).
                post(ClientResponse.class, form);
            System.out.println(response.getResponseStatus().toString());
            readForm = response.getEntity(Form.class);
            assertEquals(sampleInt2, readForm.getFirst(intParamName));
            assertEquals(sampleText2, readForm.getFirst(strParamName));
            assertEquals(sampleBool, readForm.getFirst(boolParamName));
            assertEquals(Status.OK.getStatusCode(), response.getStatus());
            response.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            fail(ex.getMessage());
        }
    }

    protected FormDataBodyPart addFileAttachment(final String fileParamName,
                                                 final File file,
                                                 final MediaType imageType) {
        FormDataBodyPart bodyPart = new FormDataBodyPart(fileParamName, file,
            imageType);
        bodyPart.getHeaders().putSingle("Content-Type", imageType.toString());
        bodyPart.getHeaders().
            putSingle("Content-Disposition",
            getContentDisposition(fileParamName, file));
        return bodyPart;
    }

    protected String getContentDisposition(String paramName,
                                           File file) {
        if (StringUtils.isBlank(paramName)) {
            throw new IllegalArgumentException();
        }
        StringBuilder builder = new StringBuilder("form-data; name=");
        builder.append(paramName);
        if (file != null) {
            builder.append("; filename=").append(file.getName());
            if (file.exists()) {
                builder.append("; size=").append(file.length());
            }
        }
        return builder.toString();
    }

    private void doTestBookServices() {
        doTestBookCreate();
        doTestBookUpdate();
        doTestBookGetByISBN();
        doTestBookDelete();
        doTestBookSearchGet();
        doTestBookSearchPost();
    }

    private void doTestPublicationServices() {
        doTestPublicationHouseCreate();
        doTestPublicationHouseUpdate();
        doTestPublicationHouseGetByUniqueShortName();
        doTestPublicationHouseDelete();
        doTestPublicationHouseSearchGet();
        doTestPublicationHouseSearchPost();
    }

    public void doTestBookCreate() {
        int count = Math.abs(new Random().nextInt()) + 1;
        if (count > 10) {
            count = 10;
        }
        System.out.println("Total creation iteration number: " + count);
        BookElement bookElement = new BookElement();
        Book book = new Book();
        bookElement.setBook(book);

        for (int j = 0; j < count; ++j) {
            for (int i = 0; i < 2; ++i) {
                final Builder type =
                    getWebResource().path("book").type("application/xml");
                type.post(bookElement);
            }
            try {
                final Builder type =
                    getWebResource().path("book").type("application/xml");
                type.post(bookElement);
                fail("Should not succeed!");
            }
//            catch(UniformInterfaceException interfaceException) {
//                try {
//                    interfaceException.getResponse().close();
//                }
//                catch(Exception ex) {
//                    fail(ex.getMessage());
//                }
//            }
            catch (Exception ex) {
            }
        }
    }

    public void doTestBookDelete() {
        String isbn = "testA";
        final String baseUri = "book";
        webResource.path(baseUri + "/" + isbn).delete();
    }

    private void doTestBookUpdate() {
        String isbn = "testA";
        WebResource resource = webResource.path("book/" + isbn);
        assertEquals(200, resource.head().getStatus());
        final BookElement element = resource.get(BookElement.class);
        assertNotNull(element);
        final Builder type = webResource.path("book").type("application/xml");
        type.put(element);
    }

    private void doTestBookGetByISBN() {
        String isbn = "testA";
        WebResource resource = webResource.path("book/" + isbn);
        assertEquals(200, resource.head().getStatus());
        final BookElement element = resource.get(BookElement.class);
        assertNotNull(element);
        final Book book = element.getBook();
        assertNotNull(book);
        assertEquals(isbn, book.getIsbn());
        assertEquals("No Name", book.getName());
    }

    private void doTestBookSearchGet() {
        MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        final String name = "Muktomon";
        final String isbn = "000-0000-000";
        map.add("name", name);
        map.add("isbn", isbn);
        WebResource resource =
            webResource.path("book").queryParams(map);
        BookElements elements = resource.get(BookElements.class);
        verifyBookElements(elements, name, isbn);
        final String nameDefault = "NO NAME";
        final String isbnDefault = "NO ISBN";
        resource = webResource.path("book");
    //elements = resource.get(BookElements.class);
    //verifyBookElements(elements, nameDefault, isbnDefault);
    }

    private void doTestBookSearchPost() {
        BookFilterElement filter = new BookFilterElement();
        BookFilter bookFilter = new BookFilter();
        filter.setBookFilter(bookFilter);
        final String name = "Muktomon";
        final String isbn = "000-0000-000";
        bookFilter.setName(name);
        bookFilter.setIsbn(isbn);
        final Builder type = webResource.path("book/search").type(
            "application/xml");
        final BookElements elements = type.post(BookElements.class, filter);
        verifyBookElements(elements, name, isbn);
    }

    private void doTestPublicationHouseDelete() {
        String uniqueShortName = "testA";
        final String baseUri = "pub-house";
        webResource.path(baseUri + "/" + uniqueShortName).delete();
    }

    private void doTestPublicationHouseGetByUniqueShortName() {
        String uniqueShortName = "testA";
        WebResource resource = webResource.path("pub-house/" +
            uniqueShortName);
        assertEquals(200, resource.head().getStatus());
        final PublicationHouseElement element = resource.get(
            PublicationHouseElement.class);
        assertNotNull(element);
        final PublicationHouse publicationHouse = element.getPublicationHouse();
        assertNotNull(publicationHouse);
        assertEquals(uniqueShortName, publicationHouse.getUniqueShortName());
        assertEquals("No Name", publicationHouse.getName());
        assertEquals("NONE@NONE", publicationHouse.getContactEmailAddress());
    }

    private void doTestPublicationHouseSearchGet() {
        MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        final String name = "Sheba Prokashoni";
        final String uniqueName = "sheba-prok";
        map.add("name", name);
        map.add("shortName", uniqueName);
        WebResource resource =
            webResource.path("pub-house").queryParams(map);
        PublicationHouseElements elements = resource.get(
            PublicationHouseElements.class);
        verifyPublicationHouseElements(elements, name, uniqueName);
        final String nameDefault = "NO NAME";
        final String uniqueNameDefault = "NO SHORT NAME";
        resource =
            webResource.path("pub-house");
        elements = resource.get(
            PublicationHouseElements.class);
        verifyPublicationHouseElements(elements, nameDefault, uniqueNameDefault);
    }

    private void doTestPublicationHouseSearchPost() {
        PublicationHouseFilterElement filter =
            new PublicationHouseFilterElement();
        PublicationHouseFilter publicationHouseFilter =
            new PublicationHouseFilter();
        filter.setPublicationHouseFilter(publicationHouseFilter);
        final String name = "Sheba Prokashoni";
        final String uniqueName = "sheba-prok";
        publicationHouseFilter.setName(name);
        publicationHouseFilter.setUniqueShortString(uniqueName);
        final Builder type =
            webResource.path("pub-house/search").type("application/xml");
        final PublicationHouseElements elements = type.post(
            PublicationHouseElements.class, filter);
        verifyPublicationHouseElements(elements, name, uniqueName);
    }

    private void doTestPublicationHouseCreate() {
        int count = Math.abs(new Random().nextInt()) + 1;
        if (count > 10) {
            count = 10;
        }
        System.out.println("Total creation iteration number: " + count);
        PublicationHouseElement houseElement = new PublicationHouseElement();
        PublicationHouse house = new PublicationHouse();
        houseElement.setPublicationHouse(house);

        for (int j = 0; j < count; ++j) {
            System.out.println("Iteration: " + j);
            for (int i = 0; i < 2; ++i) {
                System.out.println("Iteration(" + j + ") " + i);
                final Builder type =
                    getWebResource().path("pub-house").type("application/xml");
                type.post(houseElement);
            }
            try {
                final Builder type =
                    getWebResource().path("pub-house").type("application/xml");
                type.post(houseElement);
                fail("Should not succeed!");
            }//            catch(UniformInterfaceException interfaceException) {
//                try {
//                    interfaceException.getResponse().close();
//                }
//                catch(Exception ex) {
//                    fail(ex.getMessage());
//                }
//            }
            catch (Exception ex) {
            }
        }
    }

    private void doTestPublicationHouseUpdate() {
        String uniqueShortName = "testA";
        WebResource resource = webResource.path("pub-house/" +
            uniqueShortName);
        assertEquals(200, resource.head().getStatus());
        final PublicationHouseElement element = resource.get(
            PublicationHouseElement.class);
        assertNotNull(element);
        final Builder type =
            webResource.path("pub-house").type("application/xml");
        type.put(element);
    }

    private WebResource getWebResource() {
        if (webResource == null) {
            DefaultApacheHttpClientConfig clientConfig =
                new DefaultApacheHttpClientConfig();
            clientConfig.getState().setCredentials(null, null, -1, "superadmin",
                "superadmin");
            clientConfig.getProperties().put(
                ApacheHttpClientConfig.PROPERTY_PREEMPTIVE_AUTHENTICATION,
                Boolean.TRUE);
            client = ApacheHttpClient.create(clientConfig);
            webResource = client.resource(BASE_URI);
        }
        return webResource;
    }

    private void verifyBookElements(final BookElements elements,
                                    final String name,
                                    final String isbn) {
        assertTrue(elements.getBooks().size() > 0);
        for (Book book : elements.getBooks()) {
            System.out.println(book.getName());
            System.out.println(book.getIsbn());
            assertTrue(book.getName().startsWith(name));
            assertTrue(book.getIsbn().startsWith(isbn));
        }
    }

    private void verifyPublicationHouseElements(
        final PublicationHouseElements elements,
        final String name,
        final String uniqueName) {
        assertTrue(elements.getPublicationHouses().size() > 0);
        for (PublicationHouse house : elements.getPublicationHouses()) {
            assertTrue(house.getName().startsWith(name));
            assertTrue(house.getUniqueShortName().startsWith(uniqueName));
        }
    }
}
