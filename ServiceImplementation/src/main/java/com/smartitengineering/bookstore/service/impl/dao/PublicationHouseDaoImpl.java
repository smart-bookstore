/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.impl.dao;

import com.smartitengineering.bookstore.domain.PublicationHouse;

/**
 *
 * @author imyousuf
 */
public class PublicationHouseDaoImpl extends AbstractDao<PublicationHouse> {

    public PublicationHouseDaoImpl() {
        setEntityClass(PublicationHouse.class);
    }
    
}
