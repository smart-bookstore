/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.service.impl.dao;

import com.smartitengineering.bookstore.domain.Book;

/**
 *
 * @author modhu7
 */
public class BookDaoImpl extends AbstractDao<Book> {

    public BookDaoImpl() {
        setEntityClass(Book.class);
    }
    
}
