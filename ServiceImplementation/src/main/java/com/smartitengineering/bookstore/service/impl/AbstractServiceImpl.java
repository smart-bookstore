/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.impl;

import com.smartitengineering.dao.common.CommonReadDao;
import com.smartitengineering.dao.common.CommonWriteDao;
import com.smartitengineering.domain.PersistentDTO;

/**
 *
 * @author imyousuf
 */
public abstract class AbstractServiceImpl<T extends PersistentDTO> {

    private CommonReadDao<T> readDao;
    private CommonWriteDao<T> writeDao;

    protected AbstractServiceImpl() {
    }

    public CommonReadDao<T> getReadDao() {
        return readDao;        
    }

    public void setReadDao(CommonReadDao<T> readDao) {
        this.readDao = readDao;
    }

    public CommonWriteDao<T> getWriteDao() {
        return writeDao;
    }

    public void setWriteDao(CommonWriteDao<T> writeDao) {
        this.writeDao = writeDao;
    }
}
