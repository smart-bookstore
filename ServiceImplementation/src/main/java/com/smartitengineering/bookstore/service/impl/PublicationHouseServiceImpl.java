/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.impl;

import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import com.smartitengineering.bookstore.service.api.PublicationHouseService;
import com.smartitengineering.dao.common.queryparam.QueryParameter;
import com.smartitengineering.dao.common.queryparam.QueryParameterFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author modhu7
 */
public class PublicationHouseServiceImpl extends AbstractServiceImpl<PublicationHouse> implements PublicationHouseService {

    public void create(PublicationHouse publicationHouse) {
        //AbstractDaoImpl<PublicationHouse> publicationHouseInstance = new AbstractDaoImpl<PublicationHouse>();
        try {
            System.out.println("Starting..............");
            getWriteDao().save(publicationHouse);
            System.out.println("Successful............");
        } catch (Exception ex) {
        }
    }

    public void update(PublicationHouse publicationHouse) {
        getWriteDao().update(publicationHouse);
    }

    public void delete(PublicationHouse publicationHouse) {
        getWriteDao().delete(publicationHouse);
    }

    public Collection<PublicationHouse> search(PublicationHouseFilter houseFilter) {
        QueryParameter qp;
        Set setPublicationHouse = new HashSet();        
        List<QueryParameter> queryParameters = new ArrayList<QueryParameter>();
        System.out.println(houseFilter.getName() + " " + houseFilter.getUniqueShortString());
        if(!StringUtils.isEmpty(houseFilter.getName())){
            qp = QueryParameterFactory.getEqualPropertyParam("name", houseFilter.getName());
            queryParameters.add(qp);
            System.out.println("name");
        }
        if(!StringUtils.isEmpty(houseFilter.getUniqueShortString())){
            qp = QueryParameterFactory.getEqualPropertyParam("uniqueShortName", houseFilter.getUniqueShortString());
            queryParameters.add(qp);
            System.out.println("unique short name");
        }
        Collection<PublicationHouse> pubHouses;
        if(queryParameters.size() == 0){
            System.out.println("All");
            pubHouses = getReadDao().getAll();         
        }else{
            System.out.println("Not All");
            pubHouses = getReadDao().getList(queryParameters);         
            System.out.println(pubHouses);
        }
        System.out.println(pubHouses);
        if(pubHouses != null) {
            setPublicationHouse.addAll(pubHouses);
        }        
        return setPublicationHouse;
    }

    public PublicationHouse getByUniqueShortName(String uniqueShortName) {
        QueryParameter qp = QueryParameterFactory.getEqualPropertyParam("uniqueShortName", uniqueShortName);
        return getReadDao().getSingle(qp);
    }    

    public Set<PublicationHouse> getAllPublicationHouse() {
        Set setPublicationHouse = new HashSet();
        try {
            setPublicationHouse = getReadDao().getAll();
        } catch (Exception ex) {
        }
        return setPublicationHouse;
    }
}
