/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.service.impl;

import com.smartitengineering.bookstore.domain.Book;
import com.smartitengineering.bookstore.domain.filter.BookFilter;
import com.smartitengineering.bookstore.service.api.BookService;
import com.smartitengineering.dao.common.queryparam.FetchMode;
import com.smartitengineering.dao.common.queryparam.MatchMode;
import com.smartitengineering.dao.common.queryparam.QueryParameter;
import com.smartitengineering.dao.common.queryparam.QueryParameterFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
public class BookServiceImpl
        extends AbstractServiceImpl<Book>
        implements BookService {

    public void create(Book book) {
        System.out.println("I m at server");
        try {
            getWriteDao().save(book);
        } catch (Exception ex) {
            System.out.println("I m at server Excepiton" + ex.getMessage());
        }
    }

    public void update(Book book) {
        getWriteDao().update(book);
    }

    public void delete(Book book) {
        getWriteDao().delete(book);
    }

    public Collection<Book> search(BookFilter bookFilter) {
        QueryParameter qp;
        Set setPublicationbook = new HashSet();
        System.out.println("Author: " + bookFilter.getAuthor());
        System.out.println("Publication House: " + bookFilter.getPublicationHouse());
        System.out.println("Book name & ISBN: " + bookFilter.getName() + " " + bookFilter.getIsbn());
        List<QueryParameter> queryParameters = new ArrayList<QueryParameter>();
        if (!StringUtils.isEmpty(bookFilter.getName())) {
            qp = QueryParameterFactory.getStringLikePropertyParam("name", bookFilter.getName(), MatchMode.EXACT);
            queryParameters.add(qp);
            System.out.println("name");
        }
        if (!StringUtils.isEmpty(bookFilter.getIsbn())) {
            qp = QueryParameterFactory.getEqualPropertyParam("isbn", bookFilter.getIsbn());
            queryParameters.add(qp);
            System.out.println("isbn");
        }
        if (!StringUtils.isEmpty(bookFilter.getPublisher()) && StringUtils.isNumeric(bookFilter.getPublisher())) {
            System.out.println("Yes, the publisher of the filter is not null");
            qp = QueryParameterFactory.getNestedParametersParam("publisher", FetchMode.DEFAULT,
                    QueryParameterFactory.getEqualPropertyParam("id", new Integer(bookFilter.getPublisher())));
            queryParameters.add(qp);
        }
        if (!StringUtils.isEmpty(bookFilter.getAuthor()) && StringUtils.isNumeric(bookFilter.getAuthor())) {
            System.out.println("Yes, the author of the filter is not null");
            qp = QueryParameterFactory.getNestedParametersParam("authors", FetchMode.DEFAULT,
                    QueryParameterFactory.getEqualPropertyParam("id", new Integer(bookFilter.getAuthor())));
            queryParameters.add(qp);
        }
        if (!StringUtils.isEmpty(bookFilter.getPublicationHouse())) {
            System.out.println("Yes, the PublicationHouse of the filter is not null");
            qp = QueryParameterFactory.getNestedParametersParam("publisher", FetchMode.DEFAULT,
                    QueryParameterFactory.getNestedParametersParam("publicationHouse", FetchMode.DEFAULT, 
                    QueryParameterFactory.getEqualPropertyParam("uniqueShortName", bookFilter.getPublicationHouse())));
            queryParameters.add(qp);
        }
        Collection<Book> books;
        if (queryParameters.size() == 0) {
            System.out.println("All Book");
            books = getReadDao().getAll();
        } else {
            System.out.println("Not All Book");
            books = getReadDao().getList(queryParameters);
        }
        if (books != null) {
            setPublicationbook.addAll(books);
        }
        return setPublicationbook;
    }

    public Book getByISBN(String isbn) {
        QueryParameter qp = QueryParameterFactory.getEqualPropertyParam("isbn", isbn);
        return getReadDao().getSingle(qp);
    }
}
