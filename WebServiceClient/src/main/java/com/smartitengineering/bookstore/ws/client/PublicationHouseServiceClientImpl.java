/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.client;

import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import com.smartitengineering.bookstore.service.api.PublicationHouseService;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElement;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElements;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.Collection;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author imyousuf
 */
class PublicationHouseServiceClientImpl
        extends AbstractClientImpl
        implements PublicationHouseService {

    public void create(PublicationHouse publicationHouse) {
        PublicationHouseElement publicationHouseElement =
                new PublicationHouseElement();
        publicationHouseElement.setPublicationHouse(publicationHouse);
        final Builder type = getWebResource().path("pub-house").type("application/xml");
        type.post(publicationHouseElement);
    }

    public void update(PublicationHouse publicationHouse) {
        PublicationHouseElement element = new PublicationHouseElement();
        element.setPublicationHouse(publicationHouse);
        final Builder type =
                getWebResource().path("pub-house").type("application/xml");
        type.put(element);
    }

    public void delete(PublicationHouse publicationHouse) {
        String uniqueShortName = publicationHouse.getUniqueShortName();
        final String baseUri = "pub-house";
        getWebResource().path(baseUri + "/" + uniqueShortName).delete();

    }

    public Collection search(PublicationHouseFilter houseFilter) {
        MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        map.add("name", houseFilter.getName());
        map.add("shortName", houseFilter.getUniqueShortString());
        System.out.println(houseFilter.getName() + " " + houseFilter.getUniqueShortString());
        WebResource resource =
                getWebResource().path("pub-house").queryParams(map);
        final PublicationHouseElements elements = resource.get(
                PublicationHouseElements.class);
        return elements.getPublicationHouses();
    }

    public PublicationHouse getByUniqueShortName(String uniqueShortName) {
        WebResource resource = getWebResource().path("pub-house/" +
                uniqueShortName);
        final PublicationHouseElement element = resource.get(PublicationHouseElement.class);
        final PublicationHouse publicationHouse = element.getPublicationHouse();
        return publicationHouse;
    }   
}
