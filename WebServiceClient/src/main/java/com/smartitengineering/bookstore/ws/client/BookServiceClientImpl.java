/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.client;

import com.smartitengineering.bookstore.domain.Book;
import com.smartitengineering.bookstore.domain.filter.BookFilter;
import com.smartitengineering.bookstore.service.api.BookService;
import com.smartitengineering.bookstore.ws.element.BookElement;
import com.smartitengineering.bookstore.ws.element.BookElements;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.Collection;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author modhu7
 */
public class BookServiceClientImpl
        extends AbstractClientImpl
        implements BookService {

    public void create(Book book) {
        System.out.println("I m at client book service 1");
        BookElement bookElement =
                new BookElement();
        bookElement.setBook(book);
        System.out.println("I m at client book service before post");
        final Builder type = getWebResource().path("book").type("application/xml");                
        type.post(bookElement);
        System.out.println("I m at client book service after post");
    }

    public void update(Book book) {
        BookElement element = new BookElement();
        element.setBook(book);
        final Builder type =
                getWebResource().path("book").type("application/xml");
        type.put(element);
    }

    public void delete(Book book) {
        String isbn = book.getIsbn();
        final String baseUri = "book";
        getWebResource().path(baseUri + "/" + isbn).delete();
    }

    public Collection<Book> search(BookFilter bookFilter) {
        System.out.println("OK I m fine");
        MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        map.add("name", bookFilter.getName());
        map.add("isbn", bookFilter.getIsbn());
        map.add("publisher", bookFilter.getPublisher());
        map.add("author", bookFilter.getAuthor());
        map.add("publicationHouse", bookFilter.getPublicationHouse());
        
        WebResource resource =
                getWebResource().path("book").queryParams(map);
        final BookElements elements = resource.get(
                BookElements.class);
        return elements.getBooks();
    }

    public Book getByISBN(String isbn) {
        WebResource resource = getWebResource().path("book/" +
                isbn);
        final BookElement element = resource.get(BookElement.class);
        final Book book = element.getBook();
        return book;
    }
}
