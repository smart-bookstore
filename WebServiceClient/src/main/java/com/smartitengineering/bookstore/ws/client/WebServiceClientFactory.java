/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.ws.client;

import com.smartitengineering.bookstore.service.api.BookService;
import com.smartitengineering.bookstore.service.api.PublicationHouseService;

/**
 *
 * @author imyousuf
 */
public final class WebServiceClientFactory {
    
    private static PublicationHouseService publicationHouseService; 
    private static BookService bookService;
    
    private WebServiceClientFactory() {
        throw new AssertionError();
    }

    public static BookService getBookService() {
        if(bookService == null) {
            bookService = new BookServiceClientImpl();
        }
        return bookService;
    }

    
    
    public static PublicationHouseService getPublicationHouseService() {
        if(publicationHouseService == null) {
            publicationHouseService = new PublicationHouseServiceClientImpl();
        }
        return publicationHouseService;
    }
}
