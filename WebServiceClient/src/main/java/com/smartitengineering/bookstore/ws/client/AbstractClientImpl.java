/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.config.ApacheHttpClientConfig;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author imyousuf
 */
public abstract class AbstractClientImpl {

    protected static final URI BASE_URI = getBaseURI();
    private Client client;
    private WebResource webResource;
    private static String username, password;

    private static int getPort(int defaultPort) {
        String port = System.getenv("JERSEY_HTTP_PORT");
        if (null != port) {
            try {
                return Integer.parseInt(port);
            } catch (NumberFormatException e) {
            }
        }
        return defaultPort;
    }

    private static URI getBaseURI() {
        /*
         * Please note that the following code needs to change to read the
         * host, port and context from a properties file
         */
        Properties properties = new Properties();

        try {
            InputStream file = AbstractClientImpl.class.getClassLoader().getResourceAsStream(
                    "connection.properties");
            properties.load(file);
        } catch (IOException ex) {
        }
        String url = properties.getProperty("url");
        String port = properties.getProperty("port");
        String warname = properties.getProperty("warname");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        return UriBuilder.fromUri(url).port(getPort(new Integer(port))).path(warname).build();
    }

    protected AbstractClientImpl() {
    }

    public WebResource getWebResource() {
        DefaultApacheHttpClientConfig clientConfig = new DefaultApacheHttpClientConfig();
        clientConfig.getState().setCredentials(null, null, -1, LoginCenter.getUsername(), LoginCenter.getPassword());
        clientConfig.getProperties().put(ApacheHttpClientConfig.PROPERTY_PREEMPTIVE_AUTHENTICATION, 
            Boolean.TRUE);
        client = ApacheHttpClient.create(clientConfig);
        webResource = client.resource(BASE_URI);
        return webResource;
    }
}
