/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.client;

import com.smartitengineering.bookstore.domain.Author;
import com.smartitengineering.bookstore.domain.Book;
import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.Publisher;
import com.smartitengineering.bookstore.domain.filter.BookFilter;
import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import com.smartitengineering.bookstore.service.api.BookService;
import com.smartitengineering.bookstore.service.api.PublicationHouseService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.UriBuilder;
import junit.framework.TestCase;
import org.glassfish.embed.GlassFish;
import org.glassfish.embed.ScatteredWar;

/**
 *
 * @author imyousuf
 */
public class WebServiceClientTest
        extends TestCase {

    private Properties properties = new Properties();
    private static String connectionUri;
    private static String connectionPort;
    private static String warname;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    private static int getPort(int defaultPort) {
        String port = System.getenv("JERSEY_HTTP_PORT");
        if (null != port) {
            try {
                return Integer.parseInt(port);
            } catch (NumberFormatException e) {
            }
        }
        return defaultPort;
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri(connectionUri).port(getPort(new Integer(
                connectionPort))).path(warname).build();
    }
    private GlassFish glassfish;

    public WebServiceClientTest(String testName) throws IOException {
        super(testName);
    }

    @Override
    protected void setUp()
            throws Exception {
        super.setUp();
        try {
            InputStream inputStream = WebServiceClientTest.class.getClassLoader().
                    getResourceAsStream("testConfiguration.properties");
            properties.load(inputStream);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WebServiceClientTest.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
        connectionUri = properties.getProperty("uri");
        connectionPort = properties.getProperty("port");
        warname = properties.getProperty("warname");
        // Start Glassfish
        String server = properties.getProperty("server");
        if (server.equalsIgnoreCase("glassfish")) {
            glassfish = new GlassFish(getBaseURI().getPort());
            // Deploy Glassfish referencing the web.xml
            ScatteredWar war = new ScatteredWar(getBaseURI().getRawPath(),
                    new File("../WebServiceServer/src/main/webapp"),
                    new File(
                    "../WebServiceServer/src/main/webapp/WEB-INF/web.xml"),
                    Collections.singleton(new File("target/classes").toURI().
                    toURL()));
            glassfish.deploy(war);
        }
    }

    @Override
    protected void tearDown()
            throws Exception {
        super.tearDown();

        String server = properties.getProperty("server");
        if (server.equalsIgnoreCase("glassfish")) {
            glassfish.stop();
        }
    }

    public void testResources() {

        if (Boolean.valueOf(properties.getProperty("testCreatePublicationHouse"))) {
            LoginCenter.setUsername("imyousuf");
            LoginCenter.setPassword("imyousuf");
            try {
                doTestCreatePublicationHouse();
                fail("Should not succed");
            } catch (Exception e) {
            }
            LoginCenter.setUsername("modhu");
            LoginCenter.setPassword("modhu");
            doTestCreatePublicationHouse();
        }
        if (Boolean.valueOf(properties.getProperty("testGetAllPublicationHouse"))) {
            doTestGetAllPublicationHouse();
        }
        if (Boolean.valueOf(properties.getProperty(
                "testGetPublicationHouseByUniqueShortname"))) {
            doTestGetPublicationHouseByUniqueShortName();
        }

        if (Boolean.valueOf(properties.getProperty("testUpdatePublicationHouse"))) {
            LoginCenter.setUsername("modhu");
            LoginCenter.setPassword("modhu");
            try {
                doTestUpdatePublicationHouse();
                fail("Should not succed");
            } catch (Exception e) {
            }
            LoginCenter.setUsername("imyousuf");
            LoginCenter.setPassword("imyousuf");
            doTestUpdatePublicationHouse();
        }
        if (Boolean.valueOf(properties.getProperty("testDeletePublicationHouse"))) {
            LoginCenter.setUsername("modhu");
            LoginCenter.setPassword("modhu");
            try {
                doTestDelete();
                fail("Should not succed");
            } catch (Exception e) {
            }
            LoginCenter.setUsername("imyousuf");
            LoginCenter.setPassword("imyousuf");
            doTestDelete();
        }
        if (Boolean.valueOf(properties.getProperty("testCreateBook"))) {
            LoginCenter.setUsername("imyousuf");
            LoginCenter.setPassword("imyousuf");
            try {
                doTestCreateBook();
                fail("Should not succed");
            } catch (Exception e) {
            }
            LoginCenter.setUsername("modhu");
            LoginCenter.setPassword("modhu");
            doTestCreateBook();
        }
        if (Boolean.valueOf(
                properties.getProperty("testReadBookListByPublisher"))) {
            doTestReadBookListByPublisher();
        }
        if (Boolean.valueOf(properties.getProperty("testReadBookListByAuthor"))) {
            doTestReadBookListByAuthor();
        }
        if (Boolean.valueOf(properties.getProperty(
                "testReadBookListByPublicationHouse"))) {
            doTestReadBookListByPublicationHouse();
        }
        if (Boolean.valueOf(properties.getProperty("testGetAll"))) {
            doTestGetAllElement();
        }
        if (Boolean.valueOf(properties.getProperty("testDeleteBook"))) {
            LoginCenter.setUsername("modhu");
            LoginCenter.setPassword("modhu");
            try {
                doTestDeleteBook();
                fail("Should not succed");
            } catch (Exception e) {
            }
            LoginCenter.setUsername("imyousuf");
            LoginCenter.setPassword("imyousuf");
            doTestDeleteBook();
        }

        if (Boolean.valueOf(properties.getProperty("testDeleteAll"))) {
            LoginCenter.setUsername("modhu");
            LoginCenter.setPassword("modhu");
            try {
                doTestDeleteAll();
                fail("Should not succed");
            } catch (Exception e) {
            }
            LoginCenter.setUsername("imyousuf");
            LoginCenter.setPassword("imyousuf");
            doTestDeleteAll();
        }

    }

    public void doTestCreatePublicationHouse() {
        PublicationHouseService service = WebServiceClientFactory.
                getPublicationHouseService();
        PublicationHouse publicationHouse = new PublicationHouse();
        for (int i = 0; i < 10; i++) {
            publicationHouse.setName("Smart Book House-" + i);
            publicationHouse.setAddress("Mohammadpur-" + i);
            publicationHouse.setContactEmailAddress("info" + i +
                    "@smartbookhouse.com");
            publicationHouse.setUniqueShortName("smart" + i);
            service.create(publicationHouse);
        }
    }

    public void doTestGetAllPublicationHouse() {
        PublicationHouseService service = WebServiceClientFactory.
                getPublicationHouseService();
        PublicationHouseFilter houseFilter = new PublicationHouseFilter();
        houseFilter.setName("Smart Book House-1");
        final Collection<PublicationHouse> result = service.search(houseFilter);
        System.out.println(result);
        Set<PublicationHouse> setPublicationHouse =
                new HashSet<PublicationHouse>(result);
        System.out.println("the number of publication houses in search result from database = " +
                setPublicationHouse.size());
        List<PublicationHouse> list = new LinkedList<PublicationHouse>(
                setPublicationHouse);
        for (PublicationHouse house : list) {
            System.out.println(house.getName());
            System.out.println(house.getAddress());
            System.out.println(house.getContactEmailAddress());
            System.out.println(house.getUniqueShortName());
        }
        PublicationHouseFilter houseFilter1 = new PublicationHouseFilter();
        houseFilter1.setUniqueShortString("smart8");
        final Collection<PublicationHouse> result1 =
                service.search(houseFilter1);
        System.out.println(result1);
        Set<PublicationHouse> setPublicationHouse1 =
                new HashSet<PublicationHouse>(result1);
        System.out.println("the number of publication houses in search result from database = " +
                setPublicationHouse1.size());
        List<PublicationHouse> list1 = new LinkedList<PublicationHouse>(
                setPublicationHouse1);
        for (PublicationHouse house : list1) {
            System.out.println(house.getName());
            System.out.println(house.getAddress());
            System.out.println(house.getContactEmailAddress());
            System.out.println(house.getUniqueShortName());
        }
    }

    public void doTestGetPublicationHouseByUniqueShortName() {
        PublicationHouseService service = WebServiceClientFactory.
                getPublicationHouseService();


    }

    public void doTestUpdatePublicationHouse() {
        PublicationHouseService service = WebServiceClientFactory.
                getPublicationHouseService();
        PublicationHouseFilter houseFilter = new PublicationHouseFilter();

        houseFilter.setName("Smart Book House-1");
        final Collection<PublicationHouse> result = service.search(houseFilter);
        List<PublicationHouse> list = new ArrayList<PublicationHouse>(result);
        for (PublicationHouse house : list) {
            house.setName(house.getName() + "XXXXXXX");
            service.update(house);
        }
        list.clear();
        list.addAll(getAllPubHouse());
        for (PublicationHouse pubHouse : list) {
            System.out.println(pubHouse.getName());
            System.out.println(pubHouse.getAddress());
            System.out.println(pubHouse.getContactEmailAddress());
            System.out.println(pubHouse.getUniqueShortName());
        }
    }

    public Set<PublicationHouse> getAllPubHouse() {
        PublicationHouseService service = WebServiceClientFactory.
                getPublicationHouseService();
        PublicationHouseFilter houseFilter = new PublicationHouseFilter();
        final Collection<PublicationHouse> result = service.search(houseFilter);
        return new HashSet<PublicationHouse>(result);
    }

    private void doTestDelete() {
        PublicationHouseService service = WebServiceClientFactory.
                getPublicationHouseService();
        List<PublicationHouse> list = new ArrayList<PublicationHouse>(
                getAllPubHouse());
        service.delete(list.get(3));
        service.delete(list.get(7));
        list.clear();
        list.addAll(getAllPubHouse());
        for (PublicationHouse pubHouse : list) {
            System.out.println(pubHouse.getName());
            System.out.println(pubHouse.getAddress());
            System.out.println(pubHouse.getContactEmailAddress());
            System.out.println(pubHouse.getUniqueShortName());
        }
    }

    private void doTestCreateBook() {
        BookService service = WebServiceClientFactory.getBookService();

        for (int i = 0; i < 10; i++) {
            Book book = new Book();
            PublicationHouse publicationHouse = new PublicationHouse();
            publicationHouse.setName("Smart Book House-200" + i);
            publicationHouse.setAddress("Mohammadpur-200" + i);
            publicationHouse.setContactEmailAddress("info200" + i +
                    "@smartitengineering.com");
            publicationHouse.setUniqueShortName("smart200-" + i);
            Author author1 = new Author();
            author1.setName("Subrata Sen Gupta" + i);
            author1.setEmailAddress("subrata" + i + "@smartitengineering.com");
            Author author2 = new Author();
            author2.setName("Imran M Yousuf" + i);
            author2.setEmailAddress("imran" + i + "@smartitengineering.com");
            List<Author> authors = new ArrayList();
            authors.add(author1);
            authors.add(author2);
            Publisher publisher = new Publisher();
            publisher.setName("AHM Yousuf-" + i);
            publisher.setEmailAddress("ahmyousuf" + i +
                    "@smartitengineering.com");
            publisher.setPublicationHouse(publicationHouse);
            book.setName("The Art Of IT-" + i);
            book.setIsbn("123-4567-890-" + i);
            book.setAuthors(new HashSet<Author>(authors));
            book.setPublisher(publisher);
            System.out.println(book.getIsbn());
            service.create(book);
        }



    /*for (int i = 0; i < 10; i++) {
    publicationHouse.setName("Smart Book House-" + i);
    publicationHouse.setAddress("Mohammadpur-" + i);
    publicationHouse.setContactEmailAddress("info" + i + "@smartbookhouse.com");
    publicationHouse.setUniqueShortName("smart" + i);
    service.create(publicationHouse);
    }*/
    }

    public Set<Book> getAllBook() {
        BookService service = WebServiceClientFactory.getBookService();
        BookFilter bookFilter = new BookFilter();
        final Collection<Book> result = service.search(bookFilter);
        return new HashSet<Book>(result);
    }

    private void doTestDeleteAll() {
        BookService bookService = WebServiceClientFactory.getBookService();
        PublicationHouseService houseService = WebServiceClientFactory.
                getPublicationHouseService();
        List<Book> listBook = new ArrayList<Book>(getAllBook());
        System.out.println("Deleting the books ............ " + listBook.size());
        for (Book book : listBook) {
            bookService.delete(book);
        }
        List<PublicationHouse> listHouse = new ArrayList<PublicationHouse>(
                getAllPubHouse());
        System.out.println("Deleting the publication house ............ " +
                listHouse.size());
        for (PublicationHouse house : listHouse) {
            houseService.delete(house);
        }
    }

    private void doTestDeleteBook() {
        BookService service = WebServiceClientFactory.getBookService();
        List<Book> list = new ArrayList<Book>(getAllBook());
        service.delete(list.get(3));

        list.clear();
        list.addAll(getAllBook());
        for (Book book : list) {
            System.out.println(book.getName());
            System.out.println(book.getIsbn());
        }
    }

    private void doTestReadBookListByAuthor() {
        BookService service = WebServiceClientFactory.getBookService();
        BookFilter bookFilter = new BookFilter();
        List<Book> listBook = new ArrayList<Book>(getAllBook());
        List<Author> listAuthors = new ArrayList<Author>(listBook.get(4).
                getAuthors());
        bookFilter.setAuthor(listAuthors.get(1).getId().toString());
        System.out.println(listAuthors.get(1).getId().toString());
        System.out.println(listAuthors.get(1).getId().toString());
        System.out.println(listAuthors.get(1).getId().toString());
        Collection<Book> result = service.search(bookFilter);
        List<Book> list = new ArrayList<Book>(result);
        System.out.println("Starting search with Author book 4 author 1");
        System.out.println(list.size());
        for (Book book : list) {
            System.out.println(book.getName());
            System.out.println(book.getIsbn());
            System.out.println(new ArrayList<Author>(book.getAuthors()).get(1).
                    getName());
            System.out.println(book.getPublisher().getName());
            System.out.println(
                    book.getPublisher().getPublicationHouse().getName());
        }
    }

    private void doTestReadBookListByPublicationHouse() {
        BookService service = WebServiceClientFactory.getBookService();
        BookFilter bookFilter = new BookFilter();
        List<Book> listBook = new ArrayList<Book>(getAllBook());
        bookFilter.setPublicationHouse("smart200-7");

        Collection<Book> result = service.search(bookFilter);

        List<Book> list = new ArrayList<Book>(result);
        System.out.println("Starting search");
        System.out.println(list.size());
        for (Book book : list) {
            System.out.println(book.getName());
            System.out.println(book.getIsbn());
            System.out.println(book.getAuthors().size());
            System.out.println(book.getPublisher().getName());
            System.out.println(
                    book.getPublisher().getPublicationHouse().getName());
            System.out.println(book.getPublisher().getPublicationHouse().
                    getUniqueShortName());
        }

    }

    private void doTestReadBookListByPublisher() {
        BookService service = WebServiceClientFactory.getBookService();
        BookFilter bookFilter = new BookFilter();
        List<Book> listBook = new ArrayList<Book>(getAllBook());
        bookFilter.setPublisher(
                listBook.get(5).getPublisher().getId().toString());
        System.out.println("This is for special search");
        System.out.println(listBook.get(5).getPublisher().getId());

        Collection<Book> result = service.search(bookFilter);
        List<Book> list = new ArrayList<Book>(result);
        System.out.println("Starting search");
        System.out.println(list.size());
        for (Book book : list) {
            System.out.println(book.getName());
            System.out.println(book.getIsbn());
            System.out.println(book.getAuthors().size());
            System.out.println(book.getPublisher().getName());
            System.out.println(
                    book.getPublisher().getPublicationHouse().getName());

        }
    }

    private void doTestGetAllElement() {
        List<PublicationHouse> list = new ArrayList<PublicationHouse>(
                getAllPubHouse());
        System.out.println("The number of total houses " + list.size());
        for (PublicationHouse house : list) {
            System.out.println(house.getName());
            System.out.println(house.getUniqueShortName());

        }
        List<Book> listBook = new ArrayList<Book>(getAllBook());
        System.out.println("The number of total books " + listBook.size());
        for (Book book : listBook) {
            System.out.println(book.getName());
            System.out.println(book.getIsbn());
            System.out.println(book.getAuthors().size());
            System.out.println(book.getPublisher().getName());
            System.out.println(book.getPublisher().getPublicationHouse());

        }

    }
}
