/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.domain;

import com.smartitengineering.domain.AbstractPersistentDTO;

/**
 *
 * @author modhu7
 */
public class PublicationHouse extends AbstractPersistentDTO<PublicationHouse> {

    private String name;
    private String address;
    private String contactEmailAddress;
    private String uniqueShortName;

    public String getAddress() {
        return address;
    }

    public String getContactEmailAddress() {
        return contactEmailAddress;
    }

    public String getName() {
        return name;
    }

    public String getUniqueShortName() {
        return uniqueShortName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContactEmailAddress(String contactEmailAddress) {
        this.contactEmailAddress = contactEmailAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUniqueShortName(String uniqueShortName) {
        this.uniqueShortName = uniqueShortName;
    }

    public boolean isValid() {
        return true;
    }
}
