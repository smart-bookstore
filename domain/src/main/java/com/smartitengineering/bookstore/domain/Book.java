/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.domain;

import com.smartitengineering.domain.AbstractPersistentDTO;
import java.util.Set;

/**
 *
 * @author modhu7
 */
public class Book extends AbstractPersistentDTO<Book> {

    private String Name;
    private Set<Author> authors;
    private Publisher publisher;
    private String isbn;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public boolean isValid() {
        return true;
    }
}
