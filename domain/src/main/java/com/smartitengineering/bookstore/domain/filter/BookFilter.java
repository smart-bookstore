/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.domain.filter;

import com.smartitengineering.bookstore.domain.Author;
import com.smartitengineering.bookstore.domain.PublicationHouse;
import com.smartitengineering.bookstore.domain.Publisher;

/**
 *
 * @author modhu7
 */
public class BookFilter {
    private String name;
    private String isbn;
    private String author;
    private String publisher;
    private String publicationHouse;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublicationHouse() {
        return publicationHouse;
    }

    public void setPublicationHouse(String publicationHouse) {
        this.publicationHouse = publicationHouse;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    
    
    

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
