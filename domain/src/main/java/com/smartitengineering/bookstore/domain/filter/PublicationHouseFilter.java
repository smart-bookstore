/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.domain.filter;

/**
 *
 * @author imyousuf
 */
public class PublicationHouseFilter {
    private String uniqueShortString;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueShortString() {
        return uniqueShortString;
    }

    public void setUniqueShortString(String uniqueShortString) {
        this.uniqueShortString = uniqueShortString;
    }
}
