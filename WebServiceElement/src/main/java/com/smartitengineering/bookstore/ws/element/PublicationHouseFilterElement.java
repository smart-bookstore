/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.ws.element;

import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author imyousuf
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PublicationHouseFilterElement {
    private PublicationHouseFilter publicationHouseFilter;

    public PublicationHouseFilter getPublicationHouseFilter() {
        return publicationHouseFilter;
    }

    public void setPublicationHouseFilter(PublicationHouseFilter publicationHouseFilter) {
        this.publicationHouseFilter = publicationHouseFilter;
    }

}
