/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.ws.element;

import com.smartitengineering.bookstore.domain.PublicationHouse;
import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author imyousuf
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PublicationHouseElements {
    
    private Collection<PublicationHouse> publicationHouses;

    public Collection<PublicationHouse> getPublicationHouses() {
        return publicationHouses;
    }

    public void setPublicationHouses(Collection<PublicationHouse> publicationHouses) {
        this.publicationHouses = publicationHouses;
    }
}
