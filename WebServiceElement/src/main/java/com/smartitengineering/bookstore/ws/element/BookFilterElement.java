/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.ws.element;

import com.smartitengineering.bookstore.domain.filter.BookFilter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author modhu7
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class BookFilterElement {
    private BookFilter bookFilter;

    public BookFilter getBookFilter() {
        return bookFilter;
    }
    
    public void setBookFilter(BookFilter bookFilter) {
        this.bookFilter = bookFilter;
    }
    
}
