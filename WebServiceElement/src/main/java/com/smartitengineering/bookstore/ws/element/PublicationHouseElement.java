/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.bookstore.ws.element;

import com.smartitengineering.bookstore.domain.PublicationHouse;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author imyousuf
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PublicationHouseElement {
    private PublicationHouse publicationHouse;

    public PublicationHouse getPublicationHouse() {
        return publicationHouse;
    }

    public void setPublicationHouse(PublicationHouse publicationHouse) {
        this.publicationHouse = publicationHouse;
    }
}
