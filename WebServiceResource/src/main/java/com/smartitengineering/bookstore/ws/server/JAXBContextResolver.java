/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.server;

import com.smartitengineering.bookstore.ws.element.BookElement;
import com.smartitengineering.bookstore.ws.element.BookElements;
import com.smartitengineering.bookstore.ws.element.BookFilterElement;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElement;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElements;
import com.smartitengineering.bookstore.ws.element.PublicationHouseFilterElement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;

/**
 *
 * @author imyousuf
 */
@Provider
public class JAXBContextResolver implements ContextResolver<JAXBContext> {

    private final JAXBContext context;
    private final Set<Class> types;
    private final Class[] cTypes = {PublicationHouseElement.class, PublicationHouseElements.class, PublicationHouseFilterElement.class,
        BookElement.class, BookElements.class, BookFilterElement.class,
    };

    public JAXBContextResolver() throws Exception {
        this.types = new HashSet(Arrays.asList(cTypes));
        this.context = JAXBContext.newInstance(cTypes);
    }

    public JAXBContext getContext(Class<?> objectType) {
        return (types.contains(objectType)) ? context : null;
    }
}
