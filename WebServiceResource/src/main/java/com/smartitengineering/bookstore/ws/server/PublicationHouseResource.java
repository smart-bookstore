/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.server;

import com.smartitengineering.bookstore.domain.filter.PublicationHouseFilter;
import com.smartitengineering.bookstore.service.api.PublicationHouseService;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElement;
import com.smartitengineering.bookstore.ws.element.PublicationHouseElements;
import com.smartitengineering.bookstore.ws.element.PublicationHouseFilterElement;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataMultiPart;
import java.io.InputStream;
import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author imyousuf
 */
@Path("/pub-house")
@Component
@Scope(value = "singleton")
public class PublicationHouseResource {

    @Resource(name = "publicationHouseService")
    private PublicationHouseService publicationHouseService;

    @POST
    @Consumes("application/xml")
    public void create(PublicationHouseElement publicationHouseElement) {
        publicationHouseService.create(publicationHouseElement.
            getPublicationHouse());
    }

    @PUT
    @Consumes("application/xml")
    public void update(PublicationHouseElement publicationHouseElement) {
        publicationHouseService.update(publicationHouseElement.
            getPublicationHouse());
    }

    @DELETE
    @Path("{uniqueShortName}")
    @Consumes("application/xml")
    public void delete(@PathParam("uniqueShortName") String uniqueShortName) {
        publicationHouseService.delete(publicationHouseService.
            getByUniqueShortName(uniqueShortName));
    }

    @POST
    @Path("search")
    @Consumes("application/xml")
    @Produces("application/xml")
    public PublicationHouseElements search(
        PublicationHouseFilterElement filterElement) {
        PublicationHouseElements elements = new PublicationHouseElements();
        elements.setPublicationHouses(
            publicationHouseService.search(filterElement != null &&
            filterElement.getPublicationHouseFilter() != null ? filterElement.
            getPublicationHouseFilter() : new PublicationHouseFilter()));
        return elements;
    }

    @GET
    public PublicationHouseElements search(
        @DefaultValue(value = "NO NAME") @QueryParam(value = "name") final String name,
        @DefaultValue(value = "NO SHORT NAME") @QueryParam(value = "shortName") final String uniqueShortName) {
        PublicationHouseFilter filter = new PublicationHouseFilter();
        filter.setName(name);
        filter.setUniqueShortString(uniqueShortName);
        PublicationHouseFilterElement filterElement =
            new PublicationHouseFilterElement();
        filterElement.setPublicationHouseFilter(filter);
        return search(filterElement);
    }

    @GET
    @Path("{uniqueShortName}")
    @Produces("application/xml")
    public PublicationHouseElement getByUniqueShortName(
        @PathParam("uniqueShortName") String uniqueShortName) {
        PublicationHouseElement element = new PublicationHouseElement();
        element.setPublicationHouse(publicationHouseService.getByUniqueShortName(
            uniqueShortName));
        return element;
    }

    public PublicationHouseService getPublicationHouseService() {
        return publicationHouseService;
    }

    public void setPublicationHouseService(
        PublicationHouseService publicationHouseService) {
        this.publicationHouseService = publicationHouseService;
    }

    @POST
    @Path("test")
    @Consumes({MediaType.MULTIPART_FORM_DATA,
                  MediaType.APPLICATION_FORM_URLENCODED
              })
    public Response testMultipartForm(
        @FormParam("myFile") final InputStream fileStream,
        @FormParam("myFile") final FormDataContentDisposition disposition,
        @FormParam("text") final String simpleText,
        @FormParam("integer") final Integer simpleInt,
        @DefaultValue(value = "false") @FormParam("bool") final Boolean simpleBoolean) {
        if (fileStream == null && disposition == null && simpleText == null &&
            simpleInt == null && simpleBoolean == null) {
            Response response = Response.status(Status.NOT_ACCEPTABLE).build();
            return response;
        }
        System.out.println(":::::::::::::::MULTIPART TEST!:::::::::::::::");
        long size;
        long fileSize;
        String fileName = "null";
        if (disposition != null && disposition.getSize() > 0) {
            size = disposition.getSize();
            fileSize = size;
            fileName = disposition.getFileName();
        }
        else {
            size = 100;
            fileSize = -1;
        }
        long count = 0;
        if (fileStream != null) {
            byte[] buffer = new byte[(int) size];
            try {
                int read = fileStream.read(buffer);
                do {
                    count += read;
                    read = fileStream.read(buffer);
                }
                while (read > -1);
                fileStream.close();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("Read file " + fileName + " size: " + count +
            ", supplied file size: " + fileSize);
        System.out.println("Integer: " + simpleInt);
        System.out.println("String: " + simpleText);
        System.out.println("Boolean: " + simpleBoolean);
        Form form = new Form();
        form.add("fileName", fileName);
        form.add("fileSize", size);
        form.add("readFileSize", count);
        form.add("text", simpleText);
        form.add("integer", simpleInt);
        form.add("bool", simpleBoolean);
        Response response = Response.ok(form).build();
        return response;
    }
}
