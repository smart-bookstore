/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.bookstore.ws.server;

import com.smartitengineering.bookstore.domain.filter.BookFilter;
import com.smartitengineering.bookstore.service.api.BookService;
import com.smartitengineering.bookstore.ws.element.BookElement;
import com.smartitengineering.bookstore.ws.element.BookElements;
import com.smartitengineering.bookstore.ws.element.BookFilterElement;
import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author modhu7
 */
@Path("/book")
@Component
@Scope(value = "singleton")
public class BookResource {

    @Resource(name = "bookService")
    private BookService bookService;

    @POST
    @Consumes("application/xml")
    public void create(BookElement bookElement) {
        bookService.create(bookElement.getBook());
    }

    @PUT
    @Consumes("application/xml")
    public void update(BookElement bookElement) {
        bookService.update(bookElement.getBook());
    }

    @DELETE
    @Path("{isbn}")
    @Consumes("application/xml")
    public void delete(@PathParam("isbn") String isbn) {
        bookService.delete(bookService.getByISBN(isbn));
    }

    @POST
    @Path("search")
    @Consumes("application/xml")
    @Produces("application/xml")
    public BookElements search(
            BookFilterElement bookFilterElement) {
        BookElements bookElements = new BookElements();
        BookFilter bookFilter;
        if (bookFilterElement != null && bookFilterElement.getBookFilter() != null) {
            bookFilter = bookFilterElement.getBookFilter();
        } else {
            bookFilter = new BookFilter();
        }        
        bookElements.setBooks(bookService.search(bookFilter));
        return bookElements;
    }
    
    @GET
    public BookElements search(
            @DefaultValue(value = "") @QueryParam(value = "name") final String name,
            @DefaultValue(value = "") @QueryParam(value = "isbn") final String isbn,
            @DefaultValue(value = "") @QueryParam(value = "publisher") final String publisher,
            @DefaultValue(value = "") @QueryParam(value = "publicationHouse") final String publicationHouse,
            @DefaultValue(value = "") @QueryParam(value = "author") final String author) {
        BookFilter bookFilter = new BookFilter();
        bookFilter.setName(name);
        bookFilter.setIsbn(isbn);
        bookFilter.setPublisher(publisher);
        bookFilter.setAuthor(author);
        bookFilter.setPublicationHouse(publicationHouse);
        BookFilterElement bookFilterElement = new BookFilterElement();
        bookFilterElement.setBookFilter(bookFilter);
        return search(bookFilterElement);
    }

    @GET
    @Path("{isbn}")
    @Produces("application/xml")
    public BookElement getByISBN(
            @PathParam("isbn") String isbn) {
        BookElement bookElement = new BookElement();
        bookElement.setBook(bookService.getByISBN(isbn));
        return bookElement;
    }

    public BookService getBookService() {
        return bookService;
    }

    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }
}
